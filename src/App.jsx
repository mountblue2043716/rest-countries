// import { useEffect, useState } from "react";
import { Route, Routes } from "react-router-dom";
import Head from "./components/Head";
import Country from "./components/Country";
import Countries from "./components/Countries";

export default function App() {
  return (
    <>
      <Head />
      <Routes>
          <Route exact path="/" element={<Countries />}>
            
          </Route>
          <Route path="/countries/:name" element={<Country />}>
            
          </Route>
      </Routes>
    </>
  );
}
