import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const Countries = () => {
  const [searchQuery, setSearchQuery] = useState("");
  const [selectedRegion, setSelectedRegion] = useState("");
  const [countries, setCountries] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    async function getData() {
      try {
        let response = await axios.get("https://restcountries.com/v3.1/all");
        setCountries(response.data);
        setIsLoading(false);
      } catch (error) {
        console.error(error);
        setIsLoading(false);
      }
    }
    getData();
  }, []);
  const filterCountries = (countries) => {
    let filteredCountries = countries;

    // Filter by search query
    filteredCountries = filteredCountries.filter((country) =>
      country.name.common.toLowerCase().startsWith(searchQuery.toLowerCase())
    );

    // Filter by region
    if (selectedRegion !== "") {
      filteredCountries = filteredCountries.filter(
        (country) => country.region === selectedRegion
      );
    }

    return filteredCountries;
  };
  const filteredCountries = filterCountries(countries);

  return (
    <>
      <div className="container">
        <div className="search-container">
          <div className="search">
            <FontAwesomeIcon className="search-icon" icon={faMagnifyingGlass} />
            <input
              placeholder="Search for a country..."
              value={searchQuery}
              onChange={(e) => setSearchQuery(e.target.value)}
            />
          </div>
          <div className="filter">
            <select
              value={selectedRegion}
              onChange={(e) => setSelectedRegion(e.target.value)}
            >
              <option value="">Filter by Region</option>
              <option value="Africa">Africa</option>
              <option value="Americas">Americas</option>
              <option value="Asia">Asia</option>
              <option value="Europe">Europe</option>
              <option value="Oceania">Oceania</option>
            </select>
          </div>
        </div>
        <section className="countries-container">
          {isLoading ? (
            <p className="loading-message">Loading...</p>
          ) : filteredCountries.length > 0 ? (
            filteredCountries.map((country) => {
              const { cca3, name, population, region, capital, flags } =
                country;
              return (
                <article key={cca3}>
                  <div className="country-link">
                    <Link to={`/countries/${cca3}`} key={cca3}>
                      <img src={flags.png} alt="" />
                      <div className="country-details">
                        <h2>{name.common}</h2>
                        <h4>
                          Population: <span>{population}</span>
                        </h4>
                        <h4>
                          Region: <span>{region}</span>
                        </h4>
                        <h4>
                          Capital: <span>{capital ? capital : "N/A"}</span>
                        </h4>
                      </div>
                    </Link>
                  </div>
                </article>
              );
            })
          ) : (
            <p className="no-countries-message">No countries found</p>
          )}
        </section>
      </div>
    </>
  );
};

export default Countries;
