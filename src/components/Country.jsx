import { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import axios from "axios";

const Country = () => {
  const { name } = useParams();
  const [country, setCountry] = useState(null);

  useEffect(() => {
    axios
      .get(`https://restcountries.com/v3.1/alpha/${name}`)
      .then((response) => {
        setCountry(response.data[0]);
      })
      .catch((error) => {
        console.log(error);
      });
  }, [name]);

  if (!country) {
    return null; 
  }

  return (
    <>
      <div className="country-details-container">
        <Link to="/">
          <button>Back</button>
        </Link>
        <div className="details-sub-container">
          <div className="flag">
            <img src={country.flags.png} alt="" />
          </div>
          <div className="country-details-unique">
            <h4 className="country-name">{country.name.common}</h4>
            <ul className="list-items">
              <div className="left-list">
                <li><span>Native Name: </span>{country.name.common}</li>
                <li><span>Population: </span>{country.population}</li>
                <li><span>Region: </span>{country.region}</li>
                <li><span>Sub Region: </span>{country.subregion}</li>
                <li><span>Capital: </span>{country.capital && country.capital[0]}</li>
              </div>
              <div className="right-list">
                <li><span>Top Level Domain: </span>{country.tld?.[0]}</li>
                <li>
                <span>Currencies: </span>
                  {Object.values(country.currencies)
                    .map((currency) => currency.name)
                    .join(", ")}
                </li>
                <li>
                <span>Languages: </span>{Object.values(country.languages).join(", ")}
                </li>
              </div>
            </ul>
            <div className="borders"><span>Border Countries: </span>
              {country.borders && country.borders.length > 0 ? (
                country.borders.map((border) => (
                  <Link to={`/countries/${border}`} key={border}>
                    <button className="border-btn">{border}</button>
                  </Link>
                ))
              ) : (
                <span className="no-border-message">No border countries found.</span>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Country;
