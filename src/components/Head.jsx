import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon } from "@fortawesome/free-solid-svg-icons";

const Head = () => {
  return (
    <div className="head">
      <span>Where in the world?</span>
      <div className="mode">
        <img src="" alt="" />
        <span>
          <FontAwesomeIcon icon={faMoon} /> Dark Mode
        </span>
      </div>
    </div>
  );
};

export default Head;
